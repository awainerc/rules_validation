package EqualsHashCodeChecker

class TestBad {
  def hashCode(i: Nothing): Boolean = { // violation
    false
  }
  override def hashCode(): Int = 0
}
