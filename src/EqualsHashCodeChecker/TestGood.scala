package EqualsHashCodeChecker

class TestGood {
  def equals(i: TestGood): Boolean = {
    false
  }

  override def equals(i: Any): Boolean = {
    false
  }

  override def hashCode(): Int = 0
}