package MultipleStringLiteralsChecker

class TestGood {
  def doNothing(): Unit = {
    val numberDum = 11111
    val formatZero = "%010d"
    print(formatZero.format(numberDum))
    print(formatZero.format(numberDum))
    print(formatZero.format(numberDum))
  }
}