package MultipleStringLiteralsChecker

class TestGood {
  def doNothing(): Unit = {
    val numberDum = 11111
    print("%010d".format(numberDum))
    print("%010d".format(numberDum))
    print("%010d".format(numberDum))
  }
}