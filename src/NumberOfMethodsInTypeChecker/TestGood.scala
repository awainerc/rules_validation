package NumberOfMethodsInTypeChecker

class TestGood {
  def doNothing() {}

  def doNothingOne() {}

  def doNothingTwo() {}

  def doNothingThree() {}

  def doNothingFour() {}

  def doNothingFive() {}

  def doNothingSix() {}

  def doNothingSeven() {}

  def doNothingEight() {}

  def doNothingNine() {}

  def doNothingTen() {}

  def doNothingEleven() {}

  def doNothingTwelve() {}

  def doNothingThirteen() {}

  def doNothingFourteen() {}

  def doNothingFifteen() {}

}

class TestGoodTwo() extends TestGood {

  def doNothingSixteen() {}

  def doNothingSeventeen() {}

  def doNothingEighteen() {}

  def doNothingNineteen() {}

  def doNothingTwenty() {}

  def doNothingTwentyOne() {}

  def doNothingTwentyTwo() {}

  def doNothingTwentyThree() {}

  def doNothingTwentyFour() {}

  def doNothingTwentyFive() {}

  def doNothingTwentySix() {}

  def doNothingTwentySeven() {}

  def doNothingTwentyEigth() {}

  def doNothingTwentyNine() {}

  def doNothingThirty() {}
}