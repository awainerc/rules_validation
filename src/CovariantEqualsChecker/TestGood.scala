package CovariantEqualsChecker

class TestGood {
  def equals(i: TestGood): Boolean = { // violation
    false
  }

  override def equals(i: Any): Boolean = {
    false
  }

  override def hashCode(): Int = 0
}