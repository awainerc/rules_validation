package CovariantEqualsChecker

class Person(val name: String, val age: Int) extends Equals {
  override def canEqual(that: Any): Boolean =
    that.isInstanceOf[Person]

  // Intentionally avoiding the call to super.equals because no ancestor has overridden equals (see note 7 below)
  override def equals(that: Any): Boolean =
    that match {
      case person: Person =>
        ((this eq person) // optional, but highly recommended sans very specific knowledge about this exact class implementation
          || (person.canEqual(this) // optional only if this class is marked final
          && (hashCode == person.hashCode) // optional, exceptionally execution efficient if hashCode is cached, at an obvious space inefficiency tradeoff
          && ((name == person.name)
          && (age == person.age)
          )
          )
          )
      case _ =>
        false
    }

  // Intentionally avoiding the call to super.hashCode because no ancestor has overridden hashCode (see note 7 below)
  override def hashCode(): Int =
    31 * name.## + age.##
}