package CyclomaticComplexity

class TestGood {
  def setPrint(i: String): Any = {
    print(i)
  }

  def doCase(): Unit = {
    val numbers = List("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")
    numbers.foreach(setPrint)
  }
}