package NullChecker

class TestBad() {
  def doSomethingWithPath(path: String): Unit = {
    val a = null
    if (path == a) {
      print("Ok")
    }
  }
}