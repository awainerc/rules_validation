package NullChecker

class TestGood {
  def doSomethingWithPath(path: String): Unit = {
    if (path.isEmpty) {
      print("Ok")
    }
  }
}