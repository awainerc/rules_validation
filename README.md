# Titulo
## Subtitulo



**Negrita**

*Cursiva*

~~Tachado~~

Texto normal

Texto cortado   
en 2 lineas

* Lista
	* Item 1
	* Item 2

1. Lista
	1. Item 1
	2. Item 2
	


Citar un texto:
> Texto citado,
> y sigue citado



Una funcion: `function()`.

Una funcion con caracter de escape ``backtick (`)``

    Linea de codigo con 4 espacios
  
```
Linea de codigo
```
  
```javascript
var oldUnload = window.onbeforeunload;
window.onbeforeunload = function() {
    saveCoverage();
    if (oldUnload) {
        return oldUnload.apply(this, arguments);
    }
};
```
---

La pagina web es [esta](http://www.example.com/)

![Imagen1](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDV_S7X-yy38sStjXw37KrRgklcFrad3f5r13oPduBcFHTB7Bh&s, "Un gato")

[id]: https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDV_S7X-yy38sStjXw37KrRgklcFrad3f5r13oPduBcFHTB7Bh&s "Un gato"

![Referenciando imagen 1][id]


| Day     | Meal    | Price |
| --------|---------|-------|
| Monday  | pasta   | $6    |
| Tuesday | chicken | $8    |



