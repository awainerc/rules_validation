## org.scalastyle.scalariform.PublicMethodsHaveTypeChecker
Verificar que un método tiene un tipo de retorno explícito, no se deduce.

#### **Ejemplo**


Bad
```scala
def helloWorld = Action {
 req =>
 Ok("Hello World!")
 }
```

Good
```scala
def helloWorld: String = {
 val hello: String = "Hola"
 hello
 }
```

#### **Justificaci&oacute;n**
Un método público declarado en un tipo es efectivamente una declaración API. Declarar explícitamente un tipo de retorno significa que otro código que depende de ese tipo no se quebrará de forma inesperada.