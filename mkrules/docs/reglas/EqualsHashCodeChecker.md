## org.scalastyle.scalariform.EqualsHashCodeChecker

Compruebe que si una clase implementa igual o hashCode, debería implementar la otra clase.


#### **Ejemplo**
Bad
```scala
package EqualsHashCodeChecker

class TestBad {
  def hashCode(i: Nothing): Boolean = { // violation
    false
  }
  override def hashCode(): Int = 0
}
```

Good
```scala
package EqualsHashCodeChecker

class TestGood {
  def equals(i: TestGood): Boolean = {
    false
  }

  override def equals(i: Any): Boolean = {
    false
  }

  override def hashCode(): Int = 0
}
```

#### **Justificaci&oacute;n**
Definir ya sea igual o hashCode en una clase sin definir el es una fuente conocida de errores. Normalmente, cuando se define una, también se debe definir la otra.