## org.scalastyle.scalariform.UppercaseLChecker
Verifica que si se usa un literal largo, se use una L mayúscula.

#### **Ejemplo**


Bad
```scala
val longLiteral: Long = 123456846l
```

Good
```scala
 val longLiteral: Long = 123456846L
```

#### **Justificaci&oacute;n**
Una L (l) minúscula puede parecerse a una número 1 con algunas fuentes.