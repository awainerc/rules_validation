## org.scalastyle.scalariform.MagicNumberChecker

Verifica el uso de números mágicos.

#### **Ejemplo**

Bad
```scala
// First case
  val numberMagic = 4
  // Second case
  var df: DataFrame = _

  def addColumn(): DataFrame = {
    catalogDF = catalogDF.select(p.nombreTabla.substring(9,7))
    catalogDF = catalogDF.withColumn(p.descParametro, lit(8))
    catalogDF
  }
```

Good
```scala
 // First case
  val numberMagic = 4
  // Second case
  val numberSeven = 7
  val numberEight = 8
  val numberNine = 9
  var df: DataFrame = _

  def addColumn(): DataFrame = {
    catalogDF = catalogDF.select(p.nombreTabla.substring(numberNine,numberSeven))
    catalogDF = catalogDF.withColumn(p.descParametro, lit(numberEight))
    catalogDF
  }

//Por defecto solo ignora los numeros -1,0,1,2
```

#### **Justificaci&oacute;n**
Reemplazar un número mágico por una constante con nombre puede hacer que el código sea más fácil de leer y entender, y puede evitar algunos errores leves.