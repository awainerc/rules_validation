## org.scalastyle.scalariform.PackageObjectNamesChecker
Verifica que el nombres del paquete de objetos coincide con una expresión regular.

#### **Ejemplo**


Bad
```scala
package object Fruits {}
```

Good
```scala
package object fruits {}
```

#### **Justificaci&oacute;n**
La Scala style guide recomienda que los nombres de los paquetes de objetos se ajusten a ciertos estándares.