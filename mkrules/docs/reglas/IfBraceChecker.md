## org.scalastyle.scalariform.IfBraceChecker

Verifica que la sentencia if tienen llaves

#### **Ejemplo**


Good
```scala
//Si la propiedad singleLineAllowed y doubleLineAllowed esta en True permitira:
  // singleLineAllowed True
  if (valueBoolean) valueString = "verdadero" else valueString = "falso"
  // doubleLineAllowed True
  if (valueBoolean) valueString = "verdadero"
  else valueString = "falso"
//De lo contrario usar llaves
```

#### **Justificaci&oacute;n**
Algunas personas descubren que las cláusulas con llaves son más fáciles de leer.