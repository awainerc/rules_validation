## org.scalastyle.scalariform.NoWhitespaceAfterLeftBracketChecker
No hay espacios en blanco después del corchete izquierdo '['

#### **Ejemplo**


Bad
```scala
package NoWhitespaceAfterLeftBracketChecker

class TestBad() {
  val z:Array[ String] = new Array[ String](3)
}
```

Good
```scala
package NoWhitespaceAfterLeftBracketChecker

class TestGood {
  val z: Array[String] = new Array[String](3)
}
```

#### **Justificaci&oacute;n**
Si hay espacios en blanco después de un corchete izquierdo, esto puede ser confuso para el lector.