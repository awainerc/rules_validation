## org.scalastyle.scalariform.NoCloneChecker
Verifica que las clases y los objetos no definen el método clone().

#### **Ejemplo**


Bad
```scala
package NoCloneChecker

class TestBad() {
  def clone()
}
```

Good
```scala
package NoCloneChecker

class TestGood {
  def copy(): Unit ={

  }
}
```

#### **Justificaci&oacute;n**
El método de clonación es difícil de acertar. Puede utilizar el copy constructor of case classes en lugar de implementar el clon. Para más información sobre clone(), vea las páginas de Effective Java by Joshua Bloch.