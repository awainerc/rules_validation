## org.scalastyle.file.FileLineLengthChecker

Verificar el número de caracteres de una línea.


#### **Ejemplo**


Bad
```scala
import com.bbva.datioamproduct.transcendencecu.objects.fields.{Branch => bra, EpigraphNormalized => epigran, EpigraphProduct => epigrap, HierarchyBranch => hierbr}
```

Good
```scala
import com.bbva.datioamproduct.transcendencecu.objects.fields.{Branch => b, EpigraphNormalized => e, EpigraphProduct => p, HierarchyBranch => h}
```

#### **Justificaci&oacute;n**
Las líneas que son demasiado largas pueden ser difíciles de leer y el desplazamiento horizontal es molesto.