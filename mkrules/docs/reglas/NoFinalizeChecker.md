## org.scalastyle.scalariform.NoFinalizeChecker
Compruebe que las clases y los objetos no definen el método finalize().

#### **Ejemplo**


Bad
```scala
package NoFinalizeChecker

class TestBad() {
  def finalize()
}
```

Good
```scala
package NoFinalizeChecker

class TestGood {
  def exit(): Unit ={

  }
}
```

#### **Justificaci&oacute;n**
finalize() se llama cuando el objeto es basura recolectada, y la recolección de basura no está garantizada. Por lo tanto, no es prudente confiar en el código en el método finalize().