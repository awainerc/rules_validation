## org.scalastyle.scalariform.StructuralTypeChecker
Compruebe que no se utilizan tipos estructurales.

#### **Ejemplo**


Bad
```scala
def callSpeak[A <: { def speak(): Unit }](obj: A) {
 obj.speak()
 }
 
 private type FooParam = {
 val baz: List[String => String]
 def bar(a: Int, b: Int): String
 }
 
```

Good
```scala
 //Los tipos estructurales se implementan con reflexión en tiempo de ejecución, y son inherentemente menos eficaces que los tipos nominales. Los desarrolladores deberían preferir el uso de tipos nominales, a menos que los tipos estructurales proporcionen un beneficio claro.
```

#### **Justificaci&oacute;n**
Los tipos estructurales (Structural types) en Scala pueden utilizar la reflexión - esto puede tener consecuencias inesperadas en el rendimiento. Advertencia: Esta comprobación también puede recoger erróneamente el tipo lamdbas y otras construcciones similares. Esta ficha debe utilizarse con cuidado. Siempre tiene la alternativa de la comprobación de scalac checking para los tipos estructurales.