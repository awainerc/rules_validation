## org.scalastyle.scalariform.SpacesBeforePlusChecker
Verifica que el signo + esté precedido de un espacio en blanco

#### **Ejemplo**


Bad
```scala
 val cadena: String = "Hola"+"Mundo"
```

Good
```scala
 //Se complementa con SpaceAfterCommentStartChecker
 val cadena: String = "Hola" + "Mundo"
```

#### **Justificaci&oacute;n**
Una expresión con espacios alrededor de + puede ser más fácil de leer