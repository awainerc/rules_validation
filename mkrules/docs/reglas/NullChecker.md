## org.scalastyle.scalariform.NullChecker
Compruebe que no se utiliza null

#### **Ejemplo**


Bad
```scala
package NullChecker

class TestBad() {
  def doSomethingWithPath(path: String): Unit = {
    val a = null
    if (path == a) {
      print("Ok")
    }
  }
}
```

Good
```scala
package NullChecker

class TestGood {
  def doSomethingWithPath(path: String): Unit = {
    if (path.isEmpty) {
      print("Ok")
    }
  }
}
```

#### **Justificaci&oacute;n**
Scala desalienta el uso de nulos, prefiriendo Option.