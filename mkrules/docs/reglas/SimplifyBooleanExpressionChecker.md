## org.scalastyle.scalariform.SimplifyBooleanExpressionChecker
La expresión Booleana puede simplificarse.

#### **Ejemplo**


Bad
```scala
var valor: Boolean = true
if (valor == true) valor = false
```

Good
```scala
var valor: Boolean = true
if (valor) valor = false
```

#### **Justificaci&oacute;n**
Una expresión booleana que puede ser simplificada puede hacer que el código sea más fácil de leer.