## org.scalastyle.scalariform.SpaceAfterCommentStartChecker
Verifica un espacio después del inicio de un comentario.

#### **Ejemplo**


Bad
```scala
/**Funcion uno
 */
 def funcion(): Unit ={}
```

Good
```scala
/** Funcion uno
 */
 def funcion(): Unit ={}
```

#### **Justificaci&oacute;n**
Para que haya coherencia con el formato de los comentarios, deje un espacio justo después del comienzo de un comentario.