## org.scalastyle.scalariform.NoWhitespaceBeforeLeftBracketChecker
No hay espacios en blanco antes del corchete izquierdo '['

#### **Ejemplo**


Bad
```scala
package NoWhitespaceBeforeLeftBracketChecker

class TestBad() {
  val z: Array [String] = new Array [String](3)
}
```

Good
```scala
package NoWhitespaceBeforeLeftBracketChecker

class TestGood {
  val z: Array[String] = new Array[String](3)
}
```

#### **Justificaci&oacute;n**
Si hay un espacio en blanco antes de un corchete izquierdo, esto puede ser confuso para el lector.