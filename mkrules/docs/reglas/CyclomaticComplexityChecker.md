## org.scalastyle.scalariform.CyclomaticComplexityChecker

Comprueba que la complejidad ciclomática de un método supera un valor.


#### **Ejemplo**


Bad
```scala
package CyclomaticComplexity

class TestBad {
  // Cyclomatic Complexity = 10
  def doCase(): Unit = {
    val z = 1
    z match {
      case 1 => print(1)
      case 2 => print(2)
      case 3 => print(3)
      case 4 => print(4)
      case 5 => print(5)
      case 6 => print(6)
      case 7 => print(7)
      case 8 => print(8)
      case 9 => print(9)
      case 10 => print(10)
      case _ => print("n")
    }
  }
}
```

Good
```scala
package CyclomaticComplexity

class TestGood {
  def setPrint(i: String): Any = {
    print(i)
  }

  def doCase(): Unit = {
    val numbers = List("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")
    numbers.foreach(setPrint)
  }
}
```

#### **Justificaci&oacute;n**
Si el código es demasiado complejo, entonces esto puede hacer que el código sea difícil de leer.