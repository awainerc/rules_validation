## org.scalastyle.scalariform.IllegalImportsChecker

Verificar que una clase no importe ciertas clases.

#### **Ejemplo**

Bad
```scala
import sun._
import java.awt._
```

Good
```scala
//No debe importarse lo que se defina en la lista del parametro illegalImports (separada por comas)
```

#### **Justificaci&oacute;n**
El uso de algunas clases puede ser desaconsejado para un proyecto. Por ejemplo, el uso de sun._ es generalmente desaconsejado porque son internos al JDK y pueden ser cambiados.