## org.scalastyle.scalariform.ObjectNamesChecker
Verifica que el nombre de los objetos coinciden con una expresión regular.

#### **Ejemplo**


Bad
```scala
object triangulo
```

Good
```scala
object Triangulo
```

#### **Justificaci&oacute;n**
La Scala style guide recomienda que los nombres de los objetos se ajusten a ciertas normas.