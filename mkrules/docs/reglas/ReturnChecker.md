## org.scalastyle.scalariform.ReturnChecker
Verificar que el return no se utiliza.

#### **Ejemplo**


Bad
```scala
def cuadro: Int = {
 val largo: Int = 10
 return largo
 }
```

Good
```scala
def cuadro: Int = {
 val largo: Int = 10
 largo
 }
```

#### **Justificaci&oacute;n**
El uso del return no suele ser necesario en Scala. De hecho, el uso de return puede impedir un estilo de programación funcional.