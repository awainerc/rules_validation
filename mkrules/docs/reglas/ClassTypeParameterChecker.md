## org.scalastyle.scalariform.ClassTypeParameterChecker

Compruebe que las clases y los objetos no definen iguales sin anular iguales (java.lang.object).


#### **Ejemplo**


No existen Ejemplos


#### **Justificaci&oacute;n**
Definir erróneamente un método covariante igual a () sin anular un método igual a(java.lang.Object) puede producir un comportamiento inesperado del tiempo de ejecución.