## org.scalastyle.scalariform.MethodNamesChecker

Verifica que el nombre de los métodos coincida con una expresión regular.

#### **Ejemplo**

Bad
```scala
  def Setlong (): Unit = {}
  def SETTALL (): Unit = {}
  def set_width (): Unit = {}
```

Good
```scala
  def setLong (): Unit = {}
  def setTall (): Unit = {}
  def setWidth (): Unit = {}
```

#### **Justificaci&oacute;n**
La Scala style guide recomienda que los nombres de los métodos se ajusten a ciertas normas. Si los métodos están sustituyendo a otro método y el método sustituido no se puede modificar, utilice el parámetro ignoreOverride.